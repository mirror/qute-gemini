# qute-gemini

A [qutebrowser](https://qutebrowser.org) userscript that allows viewing
[Gemini](https://gemini.circumlunar.space) pages by pressing a custom key. The
pages are converted to HTML by the script and then displayed in qutebrowser.

The script borrows code from [gcat](https://github.com/aaronjanse/gcat), many
thanks to its authors!



## Installation

The
[XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)
is supported but the default directory paths are used for simplicity.

1. Copy `qute-gemini` and `qute-gemini-tab` into
   `~/.local/share/qutebrowser/userscripts/`.
2. Set custom keybindings for opening Gemini URLs by adding the following lines
   to `~/.config/qutebrowser/config.py`:

    ``` python
    # Open in the current tab
    config.bind(';g', 'hint links userscript qute-gemini')
    # Open in a new tab
    config.bind(';G', 'hint links userscript qute-gemini-tab')
    ```

    The script only processes `gemini://` URLs and opens all other URLs using
    qutebrowser, so opening a non-`gemini://` should also work.
3. Re-source the qutebrowser config by running `config-source` or restart
   qutebrowser and visit the
   [Gemini spec homepage](gemini://gemini.circumlunar.space)
   to ensure that everything works.

### Custom CSS

You can create a CSS file
`~/.local/share/qutebrowser/userscripts/qute-gemini.css` and qute-gemini will
use it to style Gemini pages shown in qutebrowser. The
[kineto](https://git.sr.ht/~sircmpwn/kineto) or
[Gemini  homepage](https://gemini.circumlunar.space) CSS styles both work well.



## Known issues

- Userscript limitations
    - Only `text/gemini` content is shown. Any other MIME types received over
      Gemini are considered an error.
    - There is no support for user input (Gemini codes 1\*).
    - There is no support for client certificates (Gemini codes 6\*).
    - Opening non-`gemini://` URLs with the userscript might not work correctly
      in rare cases.
- Qutebrowser limitations
    - Opening `gemini://` URLs from the URL bar does not work. It is only
      possible to open URL in the current page via the hints.
    - Opening `gemini://` URLs by clicking on them does not work, only opening
      them via the keybinding is possible.
    - Qutebrowser userscripts don't accept arguments. My solution to being able
      to open links in the same or a different tab was to call the script under
      a different name.



## License

Copyright 2019 Solderpunk, 2020 Aaron Janse, 2020 petedussin, 2020-2021 Sotiris Papatheodorou

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the
[GNU General Public License](https://www.gnu.org/licenses/gpl.html) as
published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

gcat is distributed under the BSD 2-Clause license.

